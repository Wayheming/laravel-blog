@extends('layouts.app')

@section('content')
    @php /** @var BlogCategory $item */use App\Models\BlogCategory; @endphp

    @if ($item->exists)
        <form method="POST" action="{{route('blog.admin.categories.update', $item->id)}}">
        @method('PATCH')
    @else
        <form method="POST" action="{{route('blog.admin.categories.store')}}">
    @endif
        @csrf
        <div class="container">
            @php /** @var ViewErrorBag $errors */use Illuminate\Support\ViewErrorBag; @endphp

            @if($errors->any())
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dissmiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{$errors->first()}}
                        </div>
                    </div>
                </div>
            @endif

            @if(session('success'))
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dissmiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {{session()->get('success')}}
                        </div>
                    </div>
                </div>
            @endif

            <div class="row justify-content-center">
                <div class="col-md-8">
                    @include('blog.admin.categories.includes.item_edit_main_col')
                </div>
                <div class="col-md-4">
                    @include('blog.admin.categories.includes.item_edit_add_col')
                </div>
            </div>
        </div>
    </form>
@endsection